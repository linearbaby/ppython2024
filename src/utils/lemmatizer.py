from .context import SoBase
from .config import config

import ctypes

class Lemmatizer(SoBase):
    def __init__(self, aot_path=config.aot_path, *args, **kwargs):
        super().__init__()
        
        ### Lemmatizer
        self.so_object.CreateLemmatizer.argtypes = [ctypes.c_char_p]
        self.so_object.CreateLemmatizer.restype = ctypes.c_void_p

        self.so_object.Lemmatize.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        self.so_object.Lemmatize.restype = ctypes.c_char_p

        self.so_object.DestroyLemmatizer.argtypes = [ctypes.c_void_p]
        self.so_object.DestroyLemmatizer.restype = None

        self.ptr = self.so_object.CreateLemmatizer(aot_path.encode("utf-8"))
        
    def __call__(self, text):
        return self.so_object.Lemmatize(self.ptr, text.encode('utf-8')).decode('utf-8')

    def __del__(self):
        self.so_object.DestroyLemmatizer(self.ptr)