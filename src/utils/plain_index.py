from .context import SoBase
from .config import config

import ctypes

class Lemmatizer(SoBase):
    def __init__(self, pb_path=config.plain_pb_path, *args, **kwargs):
        super().__init__()
        
        ### Lemmatizer
        self.so_object.CreatePlainIndexStorage.argtypes = [ctypes.c_char_p]
        self.so_object.CreatePlainIndexStorage.restype = ctypes.c_void_p
        self.ptr = self.so_object.CreatePlainIndexStorage(pb_path.encode("utf-8"))

        self.so_object.DocUrl.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocUrl.restype = ctypes.c_char_p

        self.so_object.DocName.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocName.restype = ctypes.c_char_p

        self.so_object.DocBody.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocBody.restype = ctypes.c_char_p

        self.so_object.DocDownloads.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocDownloads.restype = ctypes.c_uint

        self.so_object.DocAvgRating.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocAvgRating.restype = ctypes.c_float

        self.so_object.DocTotalRatings.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocTotalRatings.restype = ctypes.c_uint

        self.so_object.DocEmbedding.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocEmbedding.restype = ctypes.POINTER(ctypes.c_float)

        self.so_object.DocEmbeddingSz.argtypes = [ctypes.c_void_p, ctypes.c_uint]
        self.so_object.DocEmbeddingSz.restype = ctypes.c_uint

        self.so_object.DocsCount.argtypes = [ctypes.c_void_p]
        self.so_object.DocsCount.restype = ctypes.c_uint

        self.so_object.DestroyPlainIndexStorage.argtypes = [ctypes.c_void_p]
        self.so_object.DestroyPlainIndexStorage.restype = None

    def doc_url(self, doc_id):
        return self.so_object.DocUrl(self.ptr, doc_id).decode('utf-8')
    
    def doc_name(self, doc_id):
        return self.so_object.DocName(self.ptr, doc_id).decode('utf-8')

    def doc_body(self, doc_id):
        return self.so_object.DocBody(self.ptr, doc_id).decode('utf-8')

    def doc_downloads(self, doc_id):
        return self.so_object.DocDownloads(self.ptr, doc_id)

    def doc_avg_rating(self, doc_id):
        return self.so_object.DocAvgRating(self.ptr, doc_id)

    def doc_total_rating(self, doc_id):
        return self.so_object.DocTotalRatings(self.ptr, doc_id)

    def doc_embedding(self, doc_id):
        res = self.so_object.DocEmbedding(self.ptr, doc_id)
        emb_size = self.doc_embedding_sz()
        return [res[i] for i in range(emb_size)]

    def doc_embedding_sz(self, doc_id) -> int:
        return self.so_object.DocEmbeddingSz(self.ptr)

    def docs_count(self) -> int:
        return self.so_object.DocsCount(self.ptr)

    def __del__(self):
        self.so_object.DestroyPlainIndexStorage(self.ptr)