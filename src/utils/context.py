import ctypes
from .config import config

class SoBase:
    so_object = None
   
    def __new__(cls, *args, **kwargs):
        so_path = kwargs.get("so_path", config.so_path)
        if cls.so_object is None:
            if so_path is None:
                raise Exception("so not initialized")
            cls.so_object = ctypes.CDLL(so_path)

        return super().__new__(cls)
        
    def __init__(self, *args, **kwargs):
        self.so_object = SoBase.so_object

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        del self

    def __call__(self, text):
        ...

    def __del__(self):
        ...

# initialize so_object from config
SoBase()