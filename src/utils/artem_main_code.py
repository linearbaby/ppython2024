from murmurhash import MurMurHash
from qddb import QDDB
from context import SoBase

import re
import sys 
import pandas as pd


if __name__ == "__main__":
    # get input
    if len(sys.argv) > 1:
        prog_input = sys.argv[1]
    else:
        prog_input = "abobus cringus, fenomenalus ненавижу уважаю съела собаку"
    if prog_input[0] == '"':
        prog_input = prog_input[1:-1]

    # process with db
    with MurMurHash() as mhash, QDDB() as qddb:
        preprocess = ' '.join([p for p in re.split(u'[^a-zA-Zа-яА-Я_0-9]+', prog_input.upper()) if p])
        query_hash = mhash(
            preprocess
        )
        term = qddb.qddb_term_location(query_hash)
        docindex = term & 0xFFFFFFFF
        term_location = (term >> 32) & 0xFFFFFFFF
        print(preprocess, term_location, docindex, )
        docs_data = []
        for i in range(docindex):
            docs_data.append({
                "d_id": gddb.qddb_term_record_docid(term, i),
                "clicks": gddb.qddb_term_record_clicks(term, i),
                "shows": gddb.qddb_term_record_shows(term, i),
                "downloads": gddb.qddb_term_record_downloads(term, i),
            })

        if len(docs_data) == 0:
            # показать самые популярные приложения (по полю downloads)
            # я не знаю как это сделать.
            print("Информации по запросу не найдено.")
        else:
            df = pd.DataFrame.from_dict(docs_data, orient="columns")
            df["CTR"] = df['clicks'] / df['shows']
            print(df.sort_values("CTR"))
