from .context import SoBase
from .config import config

import ctypes

class MurMurHash(SoBase):
    def __init__(self, *args, **kwargs):
        super().__init__()
        
        ### Lemmatizer
        self.so_object.MurMurHash.argtypes = [ctypes.c_char_p]
        self.so_object.MurMurHash.restype = ctypes.c_uint
        
    def __call__(self, text):
        return self.so_object.MurMurHash(text.encode('utf-8'))

    def __del__(self):
        ...