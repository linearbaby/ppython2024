from .context import SoBase
from .config import config

import ctypes

class QDDB(SoBase):
    def __init__(self, pb_path=config.qddb_pb_path, *args, **kwargs):
        super().__init__()
        
        ### Lemmatizer
        self.so_object.CreateQddbStorage.argtypes = [ctypes.c_char_p]
        self.so_object.CreateQddbStorage.restype = ctypes.c_void_p
        self.ptr = self.so_object.CreateQddbStorage(pb_path.encode("utf-8"))

        self.so_object.QddbTermLocation.argtypes = [ctypes.c_void_p, ctypes.c_ulonglong]
        self.so_object.QddbTermLocation.restype = ctypes.c_ulonglong

        self.so_object.QddbTermRecord_DocId.argtypes = [ctypes.c_void_p, ctypes.c_ulong, ctypes.c_ulong]
        self.so_object.QddbTermRecord_DocId.restype = ctypes.c_ulong

        self.so_object.QddbTermRecord_Shows.argtypes = [ctypes.c_void_p, ctypes.c_ulong, ctypes.c_ulong]
        self.so_object.QddbTermRecord_Shows.restype = ctypes.c_ulong

        self.so_object.QddbTermRecord_Clicks.argtypes = [ctypes.c_void_p, ctypes.c_ulong, ctypes.c_ulong]
        self.so_object.QddbTermRecord_Clicks.restype = ctypes.c_ulong

        self.so_object.QddbTermRecord_Downloads.argtypes = [ctypes.c_void_p, ctypes.c_ulong, ctypes.c_ulong]
        self.so_object.QddbTermRecord_Downloads.restype = ctypes.c_ulong

        self.so_object.DestroyQddbStorage.argtypes = [ctypes.c_void_p]
        self.so_object.DestroyQddbStorage.restype = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        del self

    def qddb_term_location(self, th):
        return self.so_object.QddbTermLocation(self.ptr, th)

    def qddb_term_record_docid(self, term_location, docintex):
        return self.so_object.QddbTermRecord_DocId(self.ptr, term_location, docintex)

    def qddb_term_record_shows(self, term_location, docintex):
        return self.so_object.QddbTermRecord_Shows(self.ptr, term_location, docintex)

    def qddb_term_record_clicks(self, term_location, docintex):
        return self.so_object.QddbTermRecord_Clicks(self.ptr, term_location, docintex)

    def qddb_term_record_downloads(self, term_location, docintex):
        return self.so_object.QddbTermRecord_Downloads(self.ptr, term_location, docintex)

    def __del__(self):
        self.so_object.DestroyQddbStorage(self.ptr)