from omegaconf import OmegaConf

default_config = {
    "so_path": '${oc.decode:${oc.env:SO_PATH,"/root/HSE/2cem/hse_python/working_repo/search_py_emulator/bin/libsearch_py_emulator_lib.so"}}',
    "aot_path": '${oc.decode:${oc.env:AOT_PATH,"/root/HSE/2cem/hse_python/working_repo/search_py_emulator/third-party/aot"}}',
    "qddb_pb_path": '${oc.decode:${oc.env:AOT_PATH,"/root/HSE/2cem/hse_python/working_repo/search_py_emulator/data/index/qddb.pb"}}',
    "plain_pb_path": '${oc.decode:${oc.env:AOT_PATH,"/root/HSE/2cem/hse_python/working_repo/search_py_emulator/data/index/plain_index.pb"}}',
}

default_config = OmegaConf.create(default_config)
config = OmegaConf.merge(default_config)
OmegaConf.resolve(config)
OmegaConf.resolve(default_config)

######################################### VALIDATION ########################################
######################################### VALIDATION ########################################
######################################### VALIDATION ########################################
def validate_configs(
    default_config_schema, 
    config, 
    mismatched={}, 
    prefix=""
):
    """
    Recursively validate keys and types for two dictionary configs.
    """
    # Get keys for each config
    keys1 = set(default_config_schema.keys())
    keys2 = set(config.keys())
    
    # Check for keys present in one config but not the other
    extra_keys_in_config1 = keys1 - keys2
    extra_keys_in_config2 = keys2 - keys1
    
    if extra_keys_in_config1 or extra_keys_in_config2:
        for mism in extra_keys_in_config1:
            mismatched[prefix + mism] = "not declared"
        for mism in extra_keys_in_config2:
            mismatched[prefix + mism] = "extra var provided"
    
    # Check the types and values recursively
    for key in keys1.intersection(keys2):
        value1 = default_config_schema[key]
        value2 = config[key]
        
        # If both values are dictionaries, recursively validate them
        if isinstance(value1, dict) and isinstance(value2, dict):
            validate_configs(value1, value2, mismatched, prefix + key + ".")
        else:
            # Check if types match
            if type(value2) != type(value1):
                mismatched[prefix + key] = f"types mismatch: {type(value2)} != {type(value1)}"

    return mismatched

# Validate merged config against the schema
mismatched = validate_configs(OmegaConf.to_container(default_config), OmegaConf.to_container(config))
if len(mismatched) > 0:
    raise ValueError(f"Configs have different keys: {mismatched}")
